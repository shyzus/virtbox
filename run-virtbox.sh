#!/usr/bin/env bash

# First parameter is the user's uid
uid=$1
# Mount directory for images
images_dir="$(pwd)/$2"

args=(
    # Disable SELinux label to enable mounting runtime socket
    --security-opt label=disable
    # Enable legacy X11
    -v /tmp/.X11-unix/:/tmp/.X11-unix/
    -e DISPLAY=:0
    # Enable xdg runtime for wayland and pulseaudio socket  
    -v /run/user/$uid/:/run/user/$uid/
    -e XDG_RUNTIME_DIR=/run/user/$uid
    -e PULSE_SERVER=/run/user/$uid/pulse/native
    # fix XError bad access
    --ipc host
    # Add kvm access
    --device=/dev/kvm:/dev/kvm
    --userns host
    --name virtbox
    --privileged
    -v $images_dir:/var/lib/libvirt/images
    --entrypoint=bash
)

if ! command -v flatpak-spawn &> /dev/null
then
    podman run ${args[@]} -i -t virtbox
else
    flatpak-spawn --host podman run ${args[@]} virtbox
fi