#!/usr/bin/env bash

if ! command -v flatpak-spawn &> /dev/null
then
    podman build -t virtbox . 
else
    flatpak-spawn --host podman build -t virtbox . 
fi